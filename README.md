# Montgomery Converter
This is a simple GUI application that converts numbers to and from Montgomery representation. It can also be used to calculate modular addition, subtraction and multiplication using Montgomery representation. It was developed as a math class assignment.

## How to run
This program is written standard Python and uses Tkinter for its gui component. It was developed and tested using python 3.9.x. If you have Python 3.9.x with Tkinter installed on your system, simple run main.py.

## Notation
 Notation | Meaning
----------|---------
$`N`$ | input modulus
$`x`$ | input integer, $`0 \leq x \lt N`$
$`R`$ | calculated value, such that $`R=b^n`$, $`R \gt N`$ and $`gcd(b, N) = 1`$
$`[x]`$ | Montgomery representation of x such that $`[x] = x*R\ mod\ N`$
$`Redc(u)`$ | Montgomery reduction of integer $`u`$ such that $`Redc(u) = u*R^{-1}\ mod\ N`$, where  $`0 \leq u \lt R*N`$

It follows that $`[x] = Redc(x*R^2\ mod\ N)`$ and $`x = Redc([x])`$, enabling us to convert integer x to and from Montgomery representation using only Montgomery reduction. Some values must be precomputed based on given the modulus N. Then, we can calculate Redc without the need to use the modulo N operation. Therefore, subsequent conversions of multiple integers x can be evaluated very quickly, even for huge N values.

## State
This program was created in just a couple of hours of work. It is therefore entirely possible that it contains bugs. It is, however, feature complete (as the assignment only asked for efficient conversion). Extending this program to also serve as a calculator (under Montgomery representation), should be simple. Should you need such functionality, feel free to reuse this software under the terms of the specified license.
