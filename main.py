__author__ = "Tamara Tučková, Tomáš Vavro, Samuel Rosoľanka"
__credits__ = ["Tamara Tučková", "Tomáš Vavro", "Samuel Rosoľanka"]
__status__ = "Feature Complete"
__version__ = "1.0.1"
__license__ = "BSD-3-Clause"

# this was developed using python 3.9.x
# not tested against older versions

from tkinter import Tk, font, INSERT, END, StringVar
from tkinter.ttk import Frame, Notebook, Label, Button, Entry
from tkinter.scrolledtext import ScrolledText


# there really should be strings file, but we had no time to mess with that

def overrides(interface_class):
    # just to catch typos,
    # there is a package to to this,
    # but we wanted to minimize dependencies
    def overrider(method):
        assert (method.__name__ in dir(interface_class))
        return method

    return overrider


def gcd_inverse(a_val, b_val):
    """
    calculates inverse a mod b,
    that is x such that a*x = 1 (mod b)
    returns gcd, x
    """
    if a_val < b_val:
        a_val, b_val = b_val, a_val
    if b_val == 0:
        return a_val, None
    if b_val == 1:
        return a_val, 1
    q, n0, n1 = a_val // b_val, 0, 1
    # print(f"{a} | {b} | {q} | {n0} | {n1}")
    while b_val > 1:
        old_a, old_b, old_q, old_n0, old_n1 = a_val, b_val, q, n0, n1
        a_val, b_val = old_b, old_a % old_b
        if b_val == 0:
            return a_val, None
        q = a_val // b_val
        n0, n1 = old_n1, old_n0 - old_q * old_n1
        # print(f"{a} | {b} | {q} | {n0} | {n1}")
    if b_val == 0:
        return a_val, None
    return 1, n1


def gcd(num1, num2):
    if num1 < num2:
        num1, num2 = num2, num1
    if num2 == 0:
        return num1
    while num2 > 1:
        num1, num2 = num2, num1 % num2
    if num2 == 1:
        return 1
    else:  # b == 0
        return num1


class MontgomeryBase:
    def __init__(self, modulus):
        if not MontgomeryBase.is_modulus_valid(modulus):
            raise ValueError("invalid modulus!")
        self._modulus = modulus
        if modulus % 2 == 1:
            self._radix_b = 2
        else:
            self._radix_b = 3
            while True:
                if gcd(modulus, self._radix_b) == 1:
                    break
                self._radix_b += 1
        self._exponent = 1
        self._r = self._radix_b
        while self._r < self._modulus:
            self._r *= self._radix_b
            self._exponent += 1
        self._reduction_bound = self._modulus * self._r

    def _validate_reduction_input(self, u):
        if u < 0 or u >= self._reduction_bound:
            return False
        else:
            return True

    def _validate_conversion_input(self, x):
        if x < 0 or x >= self._modulus:
            return False
        else:
            return True

    def reduce(self, u):
        pass

    def convert_to_montgomery(self, x):
        pass

    def convert_from_montgomery(self, x_montgomery):
        pass

    @staticmethod
    def is_modulus_valid(modulus_n):
        if (not isinstance(modulus_n, int)) or modulus_n <= 1:
            return False
        return True


class MontgomeryBigModulus(MontgomeryBase):
    def __init__(self, modulus):
        super().__init__(modulus)
        _, self._modulus_prime = gcd_inverse(self._modulus % self._radix_b, self._radix_b)
        if self._modulus_prime is None:
            raise ValueError("couldn't calculate N' (modulus prime)")
        self._modulus_prime = (-self._modulus_prime) % self._radix_b
        self._r_squared_modulus = (self._r ** 2) % self._modulus

    def _radix_b_representation(self, input_number):
        a_representation = []
        old_q = input_number
        q = old_q // self._radix_b
        a_representation.append(input_number - (q * self._radix_b))
        while q > 0:
            old_q = q
            q = old_q // self._radix_b
            a_representation.append(old_q - (q * self._radix_b))
        while len(a_representation) < self._exponent:
            a_representation.append(0)
        return a_representation

    @overrides(MontgomeryBase)
    def reduce(self, u):
        if super()._validate_reduction_input(u):
            reduced = u
            for i in range(self._exponent):
                u_rep = self._radix_b_representation(reduced)
                tmp = (u_rep[i] * self._modulus_prime) % self._radix_b
                reduced = reduced + (tmp * self._modulus * (self._radix_b ** i))
            reduced = reduced // self._r
            if reduced >= self._modulus:
                return reduced - self._modulus
            else:
                return reduced
        else:
            return None

    @overrides(MontgomeryBase)
    def convert_to_montgomery(self, x):
        if super()._validate_conversion_input(x):
            return self.reduce(x * self._r_squared_modulus)
        else:
            return None

    @overrides(MontgomeryBase)
    def convert_from_montgomery(self, x_montgomery):
        if super()._validate_conversion_input(x_montgomery):
            return self.reduce(x_montgomery)
        else:
            return None


class MontgomerySmallModulus(MontgomeryBase):
    def __init__(self, modulus):
        super().__init__(modulus)
        _, self._r_inverse = gcd_inverse(self._modulus, self._r % self._modulus)
        if self._r_inverse is None:
            raise ValueError("couldn't invert R!")

    @overrides(MontgomeryBase)
    def reduce(self, u):
        if super()._validate_reduction_input(u):
            return (u * self._r_inverse) % self._modulus
        else:
            return None

    @overrides(MontgomeryBase)
    def convert_to_montgomery(self, x):
        if super()._validate_conversion_input(x):
            return (x * self._r) % self._modulus
        else:
            return None

    @overrides(MontgomeryBase)
    def convert_from_montgomery(self, x_montgomery):
        if super()._validate_conversion_input(x_montgomery):
            return self.reduce(x_montgomery)
        else:
            return None


class Tab:
    def __init__(self, tab_control, tab_text, desc_strings):
        self._parent_frame = Frame(tab_control)
        self._parent_frame.pack(expand=True, fill="both")
        # self._parent_frame.place(in_=tab_control, anchor="c", relx=.5, rely=0.5)
        tab_control.add(self._parent_frame, text=tab_text)
        for i, s in enumerate(desc_strings):
            Label(self._parent_frame, text=s).grid(row=i, column=0, columnspan=2)
        self._offset = len(desc_strings)

    def get_input(self):
        pass

    def set_output(self, text):
        pass


class ConvertTab(Tab):
    def __init__(self, tab_control, tab_text, button_text, button_function, desc_strings, entry_width=50):
        super().__init__(tab_control, tab_text, desc_strings)
        self._input = ScrolledText(self._parent_frame, width=entry_width)
        self._input.bind("<Button-1>", lambda event: event.widget.delete(1.0, END))
        self._output = ScrolledText(self._parent_frame, width=entry_width)
        self._output.insert(INSERT, "Tu sa objaví výstup.")
        self._output.configure(state="disabled")
        self._button = Button(self._parent_frame, text=button_text, command=button_function)
        self._input.grid(row=self._offset, column=0, padx=2)
        self._output.grid(row=self._offset, column=1, padx=2)
        self._button.grid(row=self._offset + 1, column=0, columnspan=2)

    @overrides(Tab)
    def get_input(self):
        return self._input.get("1.0", END)

    @overrides(Tab)
    def set_output(self, text):
        self._output.configure(state="normal")
        self._output.delete("1.0", END)
        self._output.insert(INSERT, text)
        self._output.configure(state="disabled")


class OperationTab(Tab):
    def __init__(self, tab_control, tab_text, button_text, button_function, desc_strings, operands, entry_width=50):
        super().__init__(tab_control, tab_text, desc_strings)
        self._inputs = {}
        self._operands = operands
        for i, op in enumerate(operands):
            inp = StringVar()
            Label(self._parent_frame, text=f"Zadajte {op}:").grid(row=self._offset + i, column=0)
            entry = Entry(self._parent_frame, textvariable=inp, width=entry_width)
            entry.grid(row=self._offset + i, column=1)
            self._inputs[op] = inp
        self._offset += len(operands)
        self._output = StringVar()
        self._output.set("Tu sa objaví výstup.")
        Label(self._parent_frame, textvariable=self._output).grid(row=self._offset, column=0, columnspan=2)
        self._button_multiply = Button(self._parent_frame, text=button_text, command=button_function)
        self._button_multiply.grid(row=self._offset + 1, column=0, columnspan=2)

    @overrides(Tab)
    def get_input(self):
        inputs = []
        for op in self._operands:
            inputs.append(self._inputs[op].get())
        return inputs

    @overrides(Tab)
    def set_output(self, text):
        self._output.set(text)


class App:
    def __init__(self, window_title="RAL zadanie 21"):
        self._IN_OUT_WIDTH = 40  # in characters

        # setup root window
        self._root = Tk()
        self._root.title(window_title)
        default_font = font.nametofont("TkDefaultFont")
        default_font.configure(size=12)
        self._root.option_add("*Font", default_font)

        # useful objects that will need to be referenced
        self._top_frame = None
        self._entry_modulus_N = None
        self._entry_modulus_N_text = None
        self._bottom_frame = None
        self._convert_to_montgomery_tab = None
        self._convert_from_montgomery_tab = None
        self._operation_add_tab = None
        self._operation_sub_tab = None
        self._operation_multiply_tab = None

        # prepare layout
        self._make_layout()

        # montgomery representation params
        self._N = None
        self._N_text = None
        self._montgomery = None

    def _make_layout(self):
        # spaghetti
        self._top_frame = Frame(self._root)
        self._top_frame.pack()

        self._bottom_frame = Frame(self._root)
        self._bottom_frame.pack()

        # top frame
        Label(self._top_frame, text="Modulus N musí byť celé číslo, N>=2.").grid(row=0, column=0, columnspan=2)
        Label(self._top_frame, text="Zadajte N:").grid(row=1, column=0)
        self._entry_modulus_N_text = StringVar()
        self._entry_modulus_N = Entry(self._top_frame, textvariable=self._entry_modulus_N_text,
                                      width=self._IN_OUT_WIDTH)
        self._entry_modulus_N.grid(row=1, column=1)

        # bottom frame tabs
        tab_control = Notebook(self._bottom_frame)
        tab_control.pack(expand=True, fill="both")

        # tab_convert_to_montgomery (conversions x -> [x])
        self._convert_to_montgomery_tab = ConvertTab(
            tab_control,
            "Prevod x na [x]",
            "x -> [x]",
            self._click_convert_to_montgomery,
            ["Tu môžete konvertovať celé čísla v rozsahu 0 až N do montgomeryho reprezenácie.",
             "Používa sa vyššie zadaná hodnota N. Čísla, ktoré chcete konvertovať, zadajte do ľavého vstupného poľa.",
             "Ak chcete konvertovať viacero čísel, zadajte každé na nový riadok."])

        # tab_convert_from_montgomery (conversion [x] -> x)
        self._convert_from_montgomery_tab = ConvertTab(
            tab_control,
            "Prevod [x] na x",
            "[x] -> x",
            self._click_convert_from_montgomery,
            ["Tu môžete konvertovať celé čísla v rozsahu 0 až N z montgomeryho reprezenácie.",
             "Používa sa vyššie zadaná hodnota N. Čísla, ktoré chcete konvertovať, zadajte do ľavého vstupného poľa.",
             "Ak chcete konvertovať viacero čísel, zadajte každé na nový riadok."])

        # tab addition
        self._operation_add_tab = OperationTab(
            tab_control,
            "Sčítanie",
            "Sčítať!",
            self._click_add,
            ["Tu môžete pomocou Montgomeryho aritmetiky spočítať výrazy v tvare x+y mod N.",
             "Používa sa vyššie zadaná hodnota N. x a y musia byť celé čísla také, že 0<=x,y<N"],
            ["x", "y"])

        # tab subtraction
        self._operation_sub_tab = OperationTab(
            tab_control,
            "Odčítanie",
            "Odčítať!",
            self._click_sub,
            ["Tu môžete pomocou Montgomeryho aritmetiky spočítať výrazy v tvare x-y mod N.",
             "Používa sa vyššie zadaná hodnota N. x a y musia byť celé čísla také, že 0<=x,y<N"],
            ["x", "y"])

        # tab_multiply (multiplication)
        self._operation_multiply_tab = OperationTab(
            tab_control,
            "Násobenie",
            "Vynásobiť!",
            self._click_multiply,
            ["Tu môžete pomocou Montgomeryho aritmetiky spočítať výrazy v tvare x*y mod N.",
             "Používa sa vyššie zadaná hodnota N. x a y musia byť celé čísla také, že 0<=x,y<N"],
            ["x", "y"])

    def _set_montgomery_params(self):
        tmp = self._entry_modulus_N_text.get()
        if tmp == "":
            return "Nezadali ste žiadnu hodnotu modulu N!"
        if tmp != self._N_text:
            if not tmp.isnumeric():
                return "Zadaný modulus N musí byť celé číslo, N>=2!"
            n_val = int(tmp)
            if not MontgomeryBase.is_modulus_valid(n_val):
                return "Zadaný modulus N musí byť celé číslo, N>=2!"
            try:
                # montgomery = MontgomeryBigModulus(n_val)
                if n_val > 1000000:
                    montgomery = MontgomeryBigModulus(n_val)
                else:
                    montgomery = MontgomerySmallModulus(n_val)
            except ValueError as ve:
                print(f"{ve}")
                return "interná chyba! Nepodarilo sa nájsť parametre pre montgomeryho redukciu!"\
                       " Prosím, oznámte nám, kde ste našli tento bug a aké vstupy ste použili."
            self._N = n_val
            self._N_text = tmp
            self._montgomery = montgomery
            return ""
        return ""

    def _click_convert_generic(self, tab, convert_func, format_str):
        if self._montgomery is None or self._N is None:
            return
        inp = tab.get_input()
        if inp == "":
            return
        lines = inp.split()
        output_lines = []
        for line in lines:
            if line.isnumeric():
                converted = convert_func(int(line))
                if converted is None:
                    output_lines.append(f"Hodnota '{line}' nie je celé číslo v rozsahu 0 až N!")
                else:
                    output_lines.append(format_str.format(val1=line, val2=converted))
            else:
                output_lines.append(f"Hodnota '{line}' nie je celé číslo!")
        output = "\n".join(output_lines)
        tab.set_output(output)

    def _click_convert_to_montgomery(self):
        msg = self._set_montgomery_params()
        if msg != "":
            self._convert_to_montgomery_tab.set_output(msg)
            return
        self._click_convert_generic(
            self._convert_to_montgomery_tab,
            None if self._montgomery is None else self._montgomery.convert_to_montgomery,
            "[{val1}] = {val2}"
        )

    def _click_convert_from_montgomery(self):
        msg = self._set_montgomery_params()
        if msg != "":
            self._convert_from_montgomery_tab.set_output(msg)
            return
        self._click_convert_generic(
            self._convert_from_montgomery_tab,
            None if self._montgomery is None else self._montgomery.convert_from_montgomery,
            "{val1} = [{val2}]"
        )

    def _click_multiply(self):
        msg = self._set_montgomery_params()
        if msg != "":
            self._operation_multiply_tab.set_output(msg)
            return
        if self._montgomery is None or self._N is None:
            return
        x, y = self._operation_multiply_tab.get_input()
        if not x.isnumeric():
            self._operation_multiply_tab.set_output(f"Hodnota x nie je celé číslo!")
            return
        if not y.isnumeric():
            self._operation_multiply_tab.set_output(f"Hodnota y nie je celé číslo!")
            return
        x_mont = self._montgomery.convert_to_montgomery(int(x))
        if x_mont is None:
            self._operation_multiply_tab.set_output(f"Hodnota x nie je v rozsahu 0 až N!")
            return
        y_mont = self._montgomery.convert_to_montgomery(int(y))
        if y_mont is None:
            self._operation_multiply_tab.set_output(f"Hodnota y nie je v rozsahu 0 až N!")
            return
        tmp = x_mont * y_mont
        c_mont = self._montgomery.reduce(tmp)
        if c_mont is None:
            self._operation_multiply_tab.set_output(f"Hodnota {tmp} nie je v rozsahu 0 až N!")
            return
        c = self._montgomery.reduce(c_mont)
        if c is None:  # this shouldn't happen
            self._operation_multiply_tab.set_output(f"Hodnota {c_mont} nie je v rozsahu 0 až N!")
            return
        self._operation_multiply_tab.set_output(f"Výsledok: {c}")

    def _click_operation_add_sub_generic(self, tab, op):
        msg = self._set_montgomery_params()
        if msg != "":
            tab.set_output(msg)
            return
        if self._montgomery is None or self._N is None:
            return
        x, y = tab.get_input()
        if not x.isnumeric():
            tab.set_output(f"Hodnota x nie je celé číslo!")
            return
        if not y.isnumeric():
            tab.set_output(f"Hodnota y nie je celé číslo!")
            return
        x_mont = self._montgomery.convert_to_montgomery(int(x))
        if x_mont is None:
            tab.set_output(f"Hodnota x nie je v rozsahu 0 až N!")
            return
        y_mont = self._montgomery.convert_to_montgomery(int(y))
        if y_mont is None:
            tab.set_output(f"Hodnota y nie je v rozsahu 0 až N!")
            return
        tmp = op(x_mont, y_mont)
        if tmp < 0:
            tmp += self._N
        c = self._montgomery.reduce(tmp)
        if c is None:  # this shouldn't happen
            tab.set_output(f"Hodnota {tmp} nie je v rozsahu 0 až N!")
            return
        tab.set_output(f"Výsledok: {c}")

    def _click_add(self):
        self._click_operation_add_sub_generic(self._operation_add_tab, lambda x, y: x + y)

    def _click_sub(self):
        self._click_operation_add_sub_generic(self._operation_sub_tab, lambda x, y: x - y)

    def run(self):
        self._root.mainloop()


if __name__ == "__main__":
    a = App()
    a.run()
